#!/bin/bash
# Script File sc2.sh
# assume that /tmp is a local not network disk
#
bigFile=/tmp/bigFile
if [ ! -e $bigFile ]; then
    # create 16MB file in /tmp with random values
    dd if=/dev/urandom of=${bigFile} bs=16M count=1
    # mac users may have to replace 16M with 16000000
fi
ls -lh $bigFile
size=4096
while [ $size -gt 1 ]; do
   echo $size $bigFile
   time wc  $bigFile
   let size=size/4
done
rm $bigFile
